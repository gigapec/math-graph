$(function() {
  $('#graph').on('load', function() {
    $('#graph').data('svgPanZoom', svgPanZoom($('#graph')[0], {'controlIconsEnabled': true, 'fit': true}));
    $($('#graph').prop('contentDocument')).find("g.node").css({'cursor': 'pointer'}).click(function() {
      $("#form-graph select").val($(this).attr("id"));
      $('#form-graph select').trigger('input');
    });
    if (screen.width > 768)
      $('html, body').animate( { scrollTop: $('#graph').offset().top + 1 }, 750);
  });
  $('#theme-img').on('load', function() {
    $('#theme-img').resize()
  });
  $("#form-graph select").on('input', function() {
    $('#theme-img').attr("src", window.images_url + $("#form-graph select").val() + '.png');
  });
  $("#form-graph").submit(function(event) {
    event.preventDefault();
    $("#graph").attr("data", $(this).attr("action") + '?' + $(this).serialize());
  });
  $("#form-graph").submit();
  $(window).resize(function() {
    var panZoom = $('#graph').data('svgPanZoom');
    if (panZoom) {
      panZoom.resize();
    }
  });
  $('#depth').on('input', function() {
    var depth = $('#depth').prop('valueAsNumber');
    if (depth == 0)
      depth = 'infinie';
    $('#depth-value').text(depth);
  });
  $('#depth').trigger('input');
});
