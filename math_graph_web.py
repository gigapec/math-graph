import io
import re
import flask
from flask_reverse_proxied import ReverseProxied
from math_graph import MathGraph

app = flask.Flask(__name__)
app.wsgi_app = ReverseProxied(app.wsgi_app)
mg = MathGraph('math-graph.csv')

@app.route('/')
def index():
    order = {'T': 1, 'M': 2, 'D': 3}
    return flask.render_template('index.html', nodes=sorted(mg.root_nodes(), key=lambda node: order[node[0]] * 1000 + int(re.sub('[^0-9]', '', node))), depth_max=mg.depth_max())

@app.route('/graph', methods=['GET', 'POST'])
def graph():
    root = flask.request.args.get('root', None)
    types = flask.request.args.getlist('types', None)
    categories = flask.request.args.getlist('categories', None)
    depth = flask.request.args.get('depth', -1, int)
    graph = io.BytesIO()
    mg.draw_graph(graph, format='svg', remove_orphans=True, root=root, depth=depth, types=types, categories=categories)
    graph.seek(io.SEEK_SET)
    return flask.send_file(graph, attachment_filename='graph.svg')
