# Description

This project is a web interface aimed at displaying a graph of dependencies for axioms, definitions, and theoremes.

# Install

Install flask

Run `FLASK_APP=math_graph_web.py FLASK_ENV=development flask run`

Go to http://localhost:5000

The .csv file format should be self explanatory.

For each axiom, definition, theorem, you can add a png image file to `static/images` folder.