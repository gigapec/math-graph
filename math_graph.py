#!/usr/bin/env python

import pygraphviz as pgv

class MathGraph:
    default_node_attributes = {'T': {'color': 'red'}, 'D': {'color': 'green', 'rank': 'min'}, 'M': {'color': 'blue', 'rank': 'source'}}
    default_edge_attributes = {'T': {'color': 'red'}, 'D': {'color': 'green'}, 'M': {'color': 'blue'}}

    def __init__(self, csv_filename, node_attributes={}, edge_attributes={}):
        self.node_attributes = self.default_node_attributes.copy()
        self.node_attributes.update(node_attributes)
        self.edge_attributes = self.default_edge_attributes.copy()
        self.edge_attributes.update(edge_attributes)
        self.graph = pgv.AGraph(directed=True)
        self.graph.node_attr.update({'style': 'filled', 'fillcolor': 'white'})
        self.parse_csv(csv_filename)

    def root_nodes(self):
        return self.graph.iternodes()

    def draw_graph(self, filename, format=None, **options):
        graph = self.graph.copy()

        if 'remove_orphans' in options and options['remove_orphans']:
            nodes = graph.nodes()
            graph.delete_nodes_from([n for n in nodes if not graph.neighbors(n)])

        if 'root' in options and options['root']:
            root = options['root']
            subgraph = pgv.AGraph(directed=True)
            subgraph.node_attr.update(graph.node_attr)
            self.subgraph_add_node(graph, subgraph, graph.get_node(root))
            subgraph.get_node(root).attr.update({'color': 'black', 'fillcolor': self.node_attributes[root[0]]['color'], 'style': 'filled', 'fontname': 'times bold', 'fontsize': '18', 'root': 'true'})
            graph = subgraph

        if 'types' in options and options['types'] and options['types'].sort() != ['D', 'M', 'T'] or 'categories' in options and options['categories'] and options['categories'].sort() != ['2D', '3D'] or 'depth' in options and options['depth'] is not None:
            root = 'root' in options and options['root'] and graph.get_node(options['root'])
            subgraph = pgv.AGraph(directed=True)
            subgraph.node_attr.update(graph.node_attr)
            types = 'types' in options and options['types']
            categories = 'categories' in options and options['categories']
            depth = 'depth' in options and options['depth'] or None
            nodes = root and [root] or [node for node in graph.iternodes() if not graph.successors(node)]
            for node in nodes:
                self.subgraph_select(graph, subgraph, node, types, categories, depth, root)
            graph = subgraph

        graph.add_subgraph([node for node in graph.nodes_iter() if node[0] == 'M'], 'M', rank='source')
        graph.add_subgraph([node for node in graph.nodes_iter() if node[0] == 'D'], 'D', rank='min')
        graph.add_subgraph([node for node in graph.nodes_iter() if node[0] == 'T'], 'T')

        graph.layout(prog='dot')
        graph.draw(filename, format)

    def parse_csv(self, csv_filename):
        f = open(csv_filename)
        f.readline()
        for l in f:
            w = l.strip().split(',')
            node = w.pop(0)
            category = w.pop(0)
            if self.graph.has_node(node):
                raise BaseException('Node duplicated: ' + node)
            self.graph.add_node(node, id=node, category=category, **self.node_attributes[node[0]])
            for dep in w:
                if not self.graph.has_node(dep):
                    if dep[0] == 'T':
                        raise BaseException('Dependencies not previously defined: ' + node + ', ' + dep)
                    else:
                        self.graph.add_node(dep, id=dep, **self.node_attributes[dep[0]])
                self.graph.add_edge(dep, node, **self.edge_attributes[dep[0]])

    def depth_max(self, node = None, checked=None, depth = 0):
        if node is None:
            nodes = self.root_nodes()
            if nodes:
                return max([self.depth_max(node, checked, depth) for node in nodes])
            else:
                return depth
        if checked is None:
            checked = []
        if node in checked:
            return depth
        checked.append(node)
        nodes = self.graph.predecessors(node)
        if nodes:
            return max([self.depth_max(node, checked, depth + 1) for node in nodes])
        else:
            return depth

    def subgraph_add_node(self, graph, subgraph, node, checked=None):
        if checked is None:
            checked = []
        if node in checked:
            return
        checked.append(node)
        subgraph.add_node(node, **node.attr)
        for pred in graph.iterpred(node):
            self.subgraph_add_node(graph, subgraph, pred, checked)
            subgraph.add_edge(pred, node, **graph.get_edge(pred, node).attr)

    def subgraph_node_select(self, node, types, categories):
        return (not types or node[0] in types) and (not categories or node[0] != 'T' or node.attr['category'] in categories)

    def subgraph_select(self, graph, subgraph, node, types, categories, depth, succ=None, checked=None):
        if checked is None:
            checked = []
        if node in checked:
            return
        checked.append(node)
        if self.subgraph_node_select(node, types, categories) or node is succ:
            subgraph.add_node(node, **node.attr)
            succ = node
        if depth is None or depth > 0:
            if depth is not None:
                depth -= 1
            for pred in graph.iterpred(node):
                self.subgraph_select(graph, subgraph, pred, types, categories, depth, succ, checked)
                if succ and self.subgraph_node_select(pred, types, categories):
                    subgraph.add_edge(pred, succ, **graph.get_edge(pred, node).attr)
